/* Package mico provides the ligatures for the Material Design icons set. */
package mico

//go:generate go run ligaturesgen.go
const (
	// Place the generated ligatures in this file.
	Output = "ligatures.go"

	// The text template to generate the ligatures file content.
	OutputTmpl = "ligaturesgen.go.txt"
)

// DecimalPrefix is prepended to Go variable names representing icons whose
// ligature starts with a decimal digit.
const DecimalPrefix = "Icon"
