module gitlab.com/fospathi/mico

go 1.20

require (
	gitlab.com/fospathi/universal v0.0.0-20230605055348-86065fb86769
	golang.org/x/exp v0.0.0-20240506185415-9bf2ced13842
)

require (
	github.com/gorilla/websocket v1.5.1 // indirect
	golang.org/x/net v0.25.0 // indirect
)

replace gitlab.com/fospathi/universal => ../universal
