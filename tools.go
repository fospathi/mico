package mico

// This tools.go file exists to let the Go build system know which imports, that
// the build system would otherwise ignore, are needed by the 'go generate' main
// package.

import (
	_ "golang.org/x/exp/slices"
)
