//go:build ignore

/*
Generate the names/ligatures of the Material Design icons in a format usable by
Go packages.
*/
package main

import (
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"text/template"
	"unicode"

	"golang.org/x/exp/slices"

	"gitlab.com/fospathi/mico"
)

// URLs to the Google github which list the ligatures and code points of the
// Material Design icons.
//
// https://github.com/google/material-design-icons/tree/master/font
const (
	regularNamesURL = "https://raw.githubusercontent.com/google/material-design-icons/master/font/MaterialIcons-Regular.codepoints"

	outlinedNamesURL = "https://raw.githubusercontent.com/google/material-design-icons/master/font/MaterialIconsOutlined-Regular.codepoints"

	roundNamesURL = "https://raw.githubusercontent.com/google/material-design-icons/master/font/MaterialIconsRound-Regular.codepoints"

	sharpNamesURL = "https://raw.githubusercontent.com/google/material-design-icons/master/font/MaterialIconsSharp-Regular.codepoints"

	twoToneNamesURL = "https://raw.githubusercontent.com/google/material-design-icons/master/font/MaterialIconsTwoTone-Regular.codepoints"
)

// Regular expressions used by [GoifyLigature].
var vRep = []struct {
	exp         *regexp.Regexp
	replacement string
}{
	{regexp.MustCompile(`(?i)api`), "API"},
	{regexp.MustCompile(`[dD]ns`), "DNS"},
	{regexp.MustCompile(`(?i)fps`), "FPS"},
	{regexp.MustCompile(`(?i)html`), "HTML"},
	{regexp.MustCompile(`(?i)http([^s]|$)`), "HTTP"},
	{regexp.MustCompile(`(?i)https`), "HTTPS"},
	{regexp.MustCompile(`(?i)ios$`), "IOS"},
	{regexp.MustCompile(`(?i)dialersip`), "DialerSIP"},
}

// IconName of a Material Design icon.
type IconName struct {
	Ligature string // The ligature used in CSS @font-face fonts.
	Go       string // A Go source code equivalent.
}

// IconNames in each stylistic variant of the Material Design icons set.
type IconNames struct {
	Regular  []IconName
	Outlined []IconName
	Round    []IconName
	Sharp    []IconName
	TwoTone  []IconName
}

// NewIconNames constructs a new IconNames.
//
// An internet connection is required.
func NewIconNames() IconNames {

	getNamesFrom := func(url string) []IconName {
		var result []IconName
		resp, err := http.Get(url)
		if err != nil {
			log.Fatalln(err)
		}
		defer resp.Body.Close()
		pairs, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}
		for _, pair := range strings.Split(string(pairs), "\n") {
			name := strings.Split(pair, " ")
			if len(name) == 0 || name[0] == "" {
				continue
			}
			goName, err := GoifyLigature(name[0])
			if err != nil {
				log.Fatalln(err)
			}
			result = append(result, IconName{Ligature: name[0], Go: goName})
		}
		result = slices.CompactFunc(result, func(left, right IconName) bool {
			return left.Ligature == right.Ligature
		})
		return result
	}

	return IconNames{
		Regular:  getNamesFrom(regularNamesURL),
		Outlined: getNamesFrom(outlinedNamesURL),
		Round:    getNamesFrom(roundNamesURL),
		Sharp:    getNamesFrom(sharpNamesURL),
		TwoTone:  getNamesFrom(twoToneNamesURL),
	}
}

// GoifyLigature maps a Material Design icon name (ligature) to a Go style
// variable name.
//
// Go variables can't start with a decimal digit so in those cases a prefix is
// used.
//
// Certain common acronyms are converted to Go's preferred all-uppercase style.
func GoifyLigature(ligature string) (string, error) {
	var (
		sb = strings.Builder{}
		vr = []rune(ligature)
	)

	if ligature == "" || len(vr) == 0 {
		return "", errors.New("error: empty name")
	}

	// Go names cannot start with a digit and need to be uppercase to be
	// exportable.
	if unicode.IsDigit(vr[0]) {
		vr = []rune(mico.DecimalPrefix + ligature)
	} else if unicode.IsLower(vr[0]) {
		vr[0] = unicode.ToUpper(vr[0])
	}

	for _, part := range strings.Split(string(vr), "_") {
		var (
			vpr = []rune(part)
			l   = len(vpr)
			psb = strings.Builder{}
		)
		if part == "" || len(vpr) == 0 {
			log.Fatalln("error: empty name part")
		}
		if unicode.IsLower(vpr[0]) {
			psb.WriteRune(unicode.ToUpper(vpr[0]))
		} else {
			psb.WriteRune(vpr[0])
		}
		for i := 1; i < l; i++ {
			psb.WriteRune(vpr[i])
		}

		s := []byte(psb.String())
		for _, rep := range vRep {
			s = rep.exp.ReplaceAll(s, []byte(rep.replacement))
		}
		sb.Write(s)
	}

	return sb.String(), nil
}

func main() {
	var (
		names = NewIconNames()
		sb    = &strings.Builder{}
		tmpl  = template.Must(template.ParseFiles(mico.OutputTmpl))
	)
	if err := tmpl.Execute(sb, []struct {
		Style string
		Names []IconName
	}{
		{"Regular", names.Regular},
		{"Outlined", names.Outlined},
		{"Round", names.Round},
		{"Sharp", names.Sharp},
		{"TwoTone", names.TwoTone},
	}); err != nil {
		log.Fatalln(err)
	}

	f, err := os.Create(mico.Output)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	_, err = f.Write([]byte(sb.String()))
	if err != nil {
		log.Fatalln(err)
	}
}
